import { GET } from '@/api/index';

export const getProtoListApi = (params) => GET('/api/front/proto/list', params);
export const getProtoDetailApi = (params) => GET('/api/front/proto', params);
export const getShopIngoApi = (params) => GET('/api/front/shop', params);

// 商品店铺结构体
export interface ShopType {
  id: number;
  logo: string;
  shop_id: string;
  shop_name: string;
  shop_score: string;
  sales: string;
  shop_score_level: number;
  shop_score_text: string;
  product_score: string;
  product_score_text: string;
  product_score_level: number;
  logistics_score: string;
  logistics_score_text: string;
  logistics_score_level: number;
  service_score: string;
  service_score_level: number;
  service_score_text: string;
}

export interface DailyStatisticsType {
  date: string;
  order_num: number;
  view_num: number;
  kol_num: number;
}

// 商品的结构体
export interface ProductType {
  shop: ShopType;
  id: number;
  product_id: bigint;
  product_id_str: string;
  title: string;
  price: number;
  coupon_price: number;
  cos_ratio: number;
  cos_fee: number;
  kol_cos_ratio: number;
  kol_cos_fee: number;
  sales: number;
  cover: string;
  imgs: string[];
  shop_name: string;
  daily_statistics: DailyStatisticsType;
  logistics_info: string;
  category_name: string;
  category_id: string;
  is_assured: boolean;
  has_sxt: boolean;
  sharable: boolean;
  comment_score: number;
  comment_num: number;
  order_num: number;
  view_num: number;
  kol_num: number;
  proto_id: number;
  in_stock: boolean;
}

// 商品的结构体
export interface ProtoType {
  id: number;
  created_at: Date;
  updated_at: Date;
  doudian_product_id: number; // 转链后的分享链接
  share_link: string; // 转链后的分享链接
  source_link: string; // 来源链接
  expire_at: bigint; // 有效期至
  link_expire_at: Date; // 链接有效期至
  selling_text: string; // 卖点信息
  delivery_address: string; // 发货地区
  not_delivery_address: string; // 不发货地区
  delivery_time: string; // 发货时间
  showcase_sales_term: number; // 橱窗销量要求
  fans_term: number; // 粉丝数量要求
  commission_ratio: number; // 自定佣金比
  commission: number; // 佣金（预估赚）
  leader_info: string; // 团长信息
  doudian_product: ProductType; // 联盟商品
  share_link_num: number; // 链接分享&复制次数
  take_num: number; // 领样次数
  stock: number; // 样品库存
  shop: ShopType;
  product_id: bigint;
  product_id_str: string;
  title: string;
  price: number;
  coupon_price: number;
  cos_ratio: number;
  cos_fee: number;
  kol_cos_ratio: number;
  kol_cos_fee: number;
  sales: number;
  cover: string;
  imgs: string[];
  shop_name: string;
  daily_statistics: DailyStatisticsType;
  logistics_info: string;
  category_name: string;
  category_id: string;
  is_assured: boolean;
  has_sxt: boolean;
  sharable: boolean;
  comment_score: number;
  comment_num: number;
  order_num: number;
  view_num: number;
  kol_num: number;
  proto_id: number;
  in_stock: boolean;
}
