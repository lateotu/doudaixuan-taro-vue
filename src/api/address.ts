import { DELETE, GET, POST, PUT } from '@/api/index';

export const getAddressApi = (params) => GET('/api/front/address/list', params);

export interface AddressType {
  id: number;
  address_id: number;
  name: string;
  full_name: string;
  parent_id: number;
}
