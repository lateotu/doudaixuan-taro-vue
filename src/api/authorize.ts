import { DELETE, POST, PUT } from '@/api/index';

/**
 * 微信授权手机号码登录
 * @param data
 */
export const wechatLoginApi = (data) => {
  return POST('/api/front/login/wechat', data);
};
export const logoutApi = () => {
  return DELETE('/api/front/logout', null);
};

/**
 * 用户授权个人微信信息作为资料
 * @param data
 */
export const authorizeWechatInfoApi = (data) => {
  return PUT('/api/front/personal/wechat/info', data);
};
