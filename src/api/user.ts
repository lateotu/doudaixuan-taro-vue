import { DELETE, GET, POST, PUT } from '@/api/index';

export const getPersonalInfoApi = (params) => GET('/api/front/personal', params);
export const updatePersonalInfoApi = (data) => PUT('/api/front/personal', data);
export const bindMobileApi = (data) => POST('/api/front/personal/mobile', data);

export const getUserAddressListApi = (params) => GET('/api/front/personal/address/list', params);
export const saveAddressApi = (data) => POST('/api/front/personal/address', data);
export const updateAddressApi = (data) => PUT('/api/front/personal/address', data);
export const delAddressApi = (data) => DELETE('/api/front/personal/address', data);

export interface UserType {
  id: number;
  avatar: string;
  nickname: string;
  uuid: string;
  mobile: string;
  email: string;
}
