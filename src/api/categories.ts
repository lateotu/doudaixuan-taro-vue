import { GET } from '@/api/index';

export const getCategoriesApi = (params) => GET('/api/front/proto/categories', params);

export interface CategoryType {
  id: number | string;
  name: string;
  icon: string;
}
