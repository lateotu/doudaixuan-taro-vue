import { GET } from '@/api/index';

export const getAgreementApi = (params) => GET('/api/front/platform/agreement', params);
export const getPlatformApi = (params) => GET('/api/front/platform', params);
