import Taro from '@tarojs/taro';

export const backOrToHome = () => {
  Taro.navigateBack({
    delta: 1,
    fail: () => {
      Taro.switchTab({ url: '/pages/tabbar/home/index' });
    },
  });
};

export const toHome = () => {
  Taro.switchTab({ url: '/pages/tabbar/home/index' });
};
