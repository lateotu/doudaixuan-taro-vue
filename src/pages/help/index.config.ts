export default {
  navigationBarTitleText: '帮助中心',
  // navigationBarBackgroundColor: "#fa2c19",
  // navigationBarTextStyle: "white"
  disableSwipeBack: true,
  enableShareTimeline: true,
  enableShareAppMessage: true,
};
