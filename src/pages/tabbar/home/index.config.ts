export default {
  // navigationBarBackgroundColor: '#fa2c19',
  // navigationBarTextStyle: 'white',
  // navigationStyle: 'custom',
  disableSwipeBack: true,
  enablePullDownRefresh: true,
  enableShareTimeline: true,
  enableShareAppMessage: true,
};
