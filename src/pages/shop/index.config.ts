export default {
  navigationBarTitleText: '店铺主页',
  navigationBarBackgroundColor: '#fa2c19',
  navigationBarTextStyle: 'white',
  disableSwipeBack: true,
  enableShareTimeline: true,
  enableShareAppMessage: true,
};
