import { createApp } from 'vue';
import {
  Button,
  Toast,
  Avatar,
  Icon,
  Tabs,
  TabPane,
  Swiper,
  SwiperItem,
  SearchBar,
  Sticky,
  Menu,
  MenuItem,
  Category,
  CategoryPane,
  Card,
  Tag,
  Row,
  Col,
  Animate,
  Price,
  FixedNav,
  Popup,
  Grid,
  GridItem,
  InfiniteLoading,
  Navbar,
  CountDown,
  TabbarItem,
  Tabbar,
  Divider,
  Cell,
  Address,
  AddressList,
  CellGroup,
  Checkbox,
  CheckboxGroup,
  Input,
  InputNumber,
  TextArea,
  Switch,
  Elevator,
  Swipe,
  Comment,
  Dialog,
  ImagePreview,
  Rate,
  Empty,
  Skeleton,
} from '@nutui/nutui-taro';

import '@nutui/nutui-taro/dist/style.css';
import './assets/iconfont/iconfont.css';
import './app.less';
import Taro, { useDidShow } from '@tarojs/taro';
import { RichText } from '@tarojs/components';
import { backOrToHome, toHome } from '@/utils/router';

Taro.offPageNotFound(() => {
  Taro.showModal({
    title: '页面找不到',
    content: '404 NotFund',
    confirmText: '首页',
    cancelText: '返回',
    success: (result) => {
      if (result.confirm) {
        toHome();
      } else {
        backOrToHome();
      }
    },
  });
});
useDidShow(() => {
  console.log('useDidShow');
});
// 注册路由中间件
const App = createApp({
  onShow(options) {
    // 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
    const updateManager = Taro.getUpdateManager();
    updateManager.onCheckForUpdate((res) => {
      console.log('是否有更新', res.hasUpdate); // 请求完新版本信息的回调 true说明有更新
    });
    updateManager.onUpdateReady(() => {
      Taro.showModal({
        title: '更新检测', // 此处可自定义提示标题
        content: '检测到新版本，是否重启小程序？', // 此处可自定义提示消息内容
        success(res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate();
          }
        },
      });
    });
    updateManager.onUpdateFailed(() => {
      // 新的版本下载失败
      Taro.showModal({
        title: '更新提示',
        content: '新版本下载失败',
        showCancel: false,
      });
    });
  },
});

App.use(Button)
  .use(Toast)
  .use(Avatar)
  .use(Icon)
  .use(Tabs)
  .use(TabPane)
  .use(Swiper)
  .use(SwiperItem)
  .use(SearchBar)
  .use(Category)
  .use(Sticky)
  .use(Menu)
  .use(MenuItem)
  .use(Card)
  .use(Tag)
  .use(Grid)
  .use(GridItem)
  .use(Row)
  .use(Col)
  .use(Price)
  .use(Popup)
  .use(Animate)
  .use(FixedNav)
  .use(Navbar)
  .use(CountDown)
  .use(Tabbar)
  .use(TabbarItem)
  .use(Cell)
  .use(CellGroup)
  .use(Divider)
  .use(Checkbox)
  .use(Address)
  .use(AddressList)
  .use(CheckboxGroup)
  .use(InfiniteLoading)
  .use(Input)
  .use(TextArea)
  .use(InputNumber)
  .use(Switch)
  .use(Elevator)
  .use(Swipe)
  .use(Comment)
  .use(Rate)
  .use(ImagePreview)
  .use(Dialog)
  .use(Skeleton)
  .use(Empty)
  .use(RichText)
  .use(Animate)
  .use(CategoryPane);

export default App;
