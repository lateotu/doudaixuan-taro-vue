## Taro vue3 小程序开发脚手架
### 抖音机构达人选品工具微信小程序 Taro+Vue3+TS+NutUI

---
### 抖带选(基础分享版)微信小程序端
#### 功能
- 选品
- 添加橱窗
- 复制链接
- 个人中心
- 帮助中心
- 达人授权
- 佣金管理
- 钱包提现
- 达人榜单
- 销售榜单
---

![](./doc/IMG_3587.PNG)
![](./doc/IMG_3588.PNG)
![](./doc/IMG_3589.PNG)

